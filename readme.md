# soal-shift-sisop-modul-1-D09-2022
## Anggota Kelompok
|NRP|Nama|Soal|
|---|----|---|
|5025201140|Ezekiel Mashal Wicaksono|Soal 1|
|5025201085|Renita Caithleen Davita| -
|5025201257|Sastiara maulikh|-

1. ## Soal 1
    * ### __register.sh__

        Disoal diminta untuk membuat `register.sh` yang berisikan program untuk membuat akun , dan juga diberikan beberapa rules dalam membuatnya. DiSourceCode saya , saya membagi menjadi 2 fungsi , yaitu `check_username` , dan `check password`. 

        * > check_username
        ```
        check_username(){
            local locUser=/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/user/user.txt
            local locLog=/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/log/log.txt
        }
        ```
        Diwali dengan mengisi directory username , yaitu ke `user.txt` . Karena ketentuan untuk username tidak ada , mangkanya tidak dibuat ketentuan khusus

        * >check_password
        ```
        check_password() {
        local locUser=/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/user/user.txt
        local locLog=/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/log/log.txt
        local lengthPassword=${#password}
        ```
        Diawali dengan mengisi directory password yang diambil dari `user.txt` . kemudian membuat syntax agar password tidak terlihat saat menulis.
        ```
        if [[ $password == $username ]]
        then
            echo "Password tidak bisa sama dengan username"
        
        elif [[ $lengthPassword -lt 8 ]]
        then
            echo "Password harus lebih dari 8 karakter"
        ```
        `if [[ $password == $username ]]` untuk mengecek apakah password sama dengan username
        `elif [[ $lengthPassword -lt 8 ]]` untuk mengecek apakah panjang password kurang dari 8

        ```
        elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
        then
        echo "Password harus terdiri dari Huruf besar , kecil , dan angka"
        
        elif grep -q $username "$locUser"
        then
            echo "Username sudah ada"
        ```
        `elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]`
        Untuk mengecek apakah password terdiri dari huruf besar , kecil ,dan angka

        `elif grep -q $username "$locUser" `untuk mengecek apakah username sudah terdaftar , karena func `check_password` berada dibagian terakhir sehingga pengecekan username dimasukan kebagian password

        ```
        else
            echo "Register successfull!"
            echo $calendar $time REGISTER:INFO User $username registered successfully >> $locLog
            echo $username $password >> $locUser
        fi
        ```
        * > main

        Jika semuanya sudah betul maka register sucessfull dan disimpan ke `log.txt` serta `user.txt`

        ```
        calendar=$(date +%D)
        time=$(date +%T)
        ```
        Calendar dan time untuk inisiasi awal penggunan waktu di `log.txt`
        ```
        printf "Masukan Username: "
        read username
        
        printf "Masukan Password: "
        read -s password
        
        check_username
        Check_password
        ```
        Input username dan password kemudian masuk ke function 
    * ### __main.sh__
        Membuat program `main.sh` yang bertujuan untuk membuat login , download picture dan att .

        * >func_login

        `func_login`bertujuan untuk memasukan username dan password yang sudah dibuat dibagian `register.sh` 

        ```
        local location="/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/user/user.txt"
        echo "Login Attempt"
        echo -n "Username: "
        read username;
        echo -n "Password: "
        lengthPassword=${#password}
        read password;
        ```
        `location="/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/user/user.txt"` menunjukan directory pengambilan data login . Kemudian dilanjut dengan input `username` dan `password` .

        ```
        if grep -qF "$password" $location
        then
        echo "LOGIN: INFO User $username logged in" >> log.txt
        echo "choose dl/att"
        read choose
        ```
        'if grep -qF "$password" $location' digunakan untuk mengecheck apakah password sesuai dengan yang ada di `user.txt` . Jika sesuai maka lanjut ke dl/att dan dimasukan dalam `log.txt`

        ```
        else
        echo "Password / Username salah"
        echo "LOGIN: ERROR failed login attempt on user $username " >> log.txt
        fi
        ```
        Jika `password / username` salah maka program berhenti dan masuk ke `log.txt`.

        * > func_unzip

        `func_unzip` digunakan jika folder dengan username yang sama ingin menambahkan jumlah foto kedalam file `.zip` nya . 
        ```
        file_count=$( shopt -s nullglob ; set -- $folder/* ; echo $#)
        ```
        `file_count` bertujuan untuk menghitung jumlah file PIC yang ada dalam file zip yang lama . 

        ```
        unzip -P $password $folder.zip 
	    func_download
        ```

        `unzip -P $password -r $folder.zip ` digunakan untuk mengunzip folder yang sudah ada . Kemudian dilanjutkan ke `func_download`

        * >func_download
        
        `func_download` digunakan untuk download foto sejumlah yang diingankan user , serta melakukan proses zipping

        ```
         for(( i=$file_count+1; i<=$n+$file_count; i++ ))
	        do
	            if [ "$i" -le 9 ]
                    then
                        photo_number=0"$i"
                    else
                        photo_number="$i"
                    fi

		        wget https://loremflickr.com/320/240 -O./$folder/PIC_$photo_number
	        done
        ```
        Looping untuk mendownload jumlah file sesuai user mau . Jika user belum pernah mendownload file maka `$file_count = 0` . 

        ```
        zip -P $password -r $folder.zip $folder
        ```
        digunakan untuk men `.zip`folder user dan menggunakan `password` login.

        ```
        rm -rf $folder
        ```
        digunakan untuk force delete folder yang digunakan sebelum zipping.

        * >main

        ```
        location="/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/user/user.txt"
        Log="/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/log.txt"
        func_login
        ```
        Inisiasi awal file `user.txt , log.txt ` dan masuk ke `func_login`

        ```
        if [ "$choose" == "dl" ];
        then
        echo "Jumlah gambar :"
        read n;

        folder=$(date +%Y-%m-%d)_$username
        ```
        jika `choose` dibagian `func_login` adalah dl maka akan diminta jumlah gambar . `folder` digunakan untuk penamaan `.zip` nya.

        ```
        if [[ -f "$folder".zip ]] #jika sudah ada foldernya
            then
                func_unzip
            else
                mkdir $folder
                file_count=0
                func_download
            fi
        ```
        jika `$folder.zip` sudah ada , maka akan masuk ke `func_unzip` . Kalau belum ada akan membuat directory baru dan `$file_count=0` , kemudian masuk ke `func_download`

        ```
        elif [ "$choose" == "att" ]
        then 
            if [[ ! -f "$Log" ]]
            then
                echo "Belum ada log"
            else
                awk -v user="$username" 'BEGIN {INFO=0} $4 == user {INFO++} 
                END {print (INFO), "Accepted login attemps are detected"}' $Log

                awk -v user="$username" 'BEGIN {ERROR=0} $8 == user {ERROR++} 
                END {print (ERROR), "Failed login attemps are detected"}' $Log
            fi
        fi
        ```
        Kalau user memilih `att` maka akan terdisplay total login berhasil serta error login menggunakan `awk`.

2. ## SS Execution

    Maaf mas saya taruh fotonya di drive ya karena saya gatau cara taruhnya di readme . Thanks mas :)

    https://drive.google.com/drive/folders/1hJnsBSfvpj-Ff8wzalil1cBiAQilsMWT?usp=sharing

3. ## Kendala

    ERROR yang sering dialami adalah bagian zipping dan juga awk , karena belum terbiasa dengan penulisannya serta cara untuk count file yang aga susah membuatnya




