# !/bin/bash
check_username(){
    local locUser=/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/user/user.txt
    local locLog=/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/log/log.txt
}

check_password() {
  local locUser=/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/user/user.txt
  local locLog=/home/ezekielmw/SISOP_PRAK_1_FINAL/soal1/log/log.txt
  local lengthPassword=${#password}

    if [[ $password == $username ]]
    then
        echo "Password tidak bisa sama dengan username"

    elif [[ $lengthPassword -lt 8 ]]
    then
        echo "Password harus lebih dari 8 karakter"

    elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
    then
	echo "Password harus terdiri dari Huruf besar , kecil , dan angka"
    
    elif grep -q $username "$locUser"
    then
        echo "Username sudah ada"

    else
      	echo "Register successfull!"
      	echo $calendar $time REGISTER:INFO User $username registered successfully >> $locLog
      	echo $username $password >> $locUser
    fi
}

calendar=$(date +%D)
time=$(date +%T)

printf "Masukan Username: "
read username

printf "Masukan Password: "
read -s password

check_username
check_password
